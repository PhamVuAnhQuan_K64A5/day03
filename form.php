<?php
$genders = array(0 => "Nam", 1 => "Nữ");
$faculties = array(
	"MAT" => "Khoa học máy tính",
	"KDL" => "Khoa học vật liệu"
);
?>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="styles.css">
    <title>Đăng ký tân sinh viên</title>
</head>
<body>
    <form>
        <div class="input-field-div">
            <div class="label-div">
                <label for="name">Họ và tên</label>
            </div>
            <input type="text" id="username" name="username">
        </div>
        <div class="input-field-div">
            <div class="label-div">
                <label for="gender">Giới tính</label>
            </div>
			<div class="gender-div">
				<?php
					for ($i = 0; $i < count($genders); ++$i) {
						echo ("
							<div>
								<input type=\"radio\" name=\"gender\">
								<label>$genders[$i]</label>
							</div>
						");
					}
				?>
			</div>
        </div>
		<div class="input-field-div">
            <div class="label-div">
                <label for="faculty">Phân khoa</label>
            </div>
			<select name="faculty" id="faculty">
				<?php
					echo "<option></option>";
					foreach ($faculties as $faculty) {
						echo "<option>$faculty</option>";
					}
				?>
			</select>
        </div>
        <div class="submit-div">
            <input type="submit" value="Đăng ký"></input>
        </div>
    </form>
</body>
</html>
